# Smallcraft Dungeondraft

SciFi Dungeondraft assets for smallcraft - space going vehicles of less than 100t in size.

These are split into an asset pack on their own because they are
large objects.

Licensed CAL-NR or CC0


